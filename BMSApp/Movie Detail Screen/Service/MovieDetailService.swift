//
//  MovieDetailService.swift
//  BMSApp
//
//  Created by Mukesh on 21/07/19.
//  Copyright © 2019 Mukesh. All rights reserved.
//

import UIKit

typealias DetailCompletion = (_ movie: MoviesModel?, _ error: String?) -> Void
typealias ActorCompletion = (_ movies: [MovieCastModel]?, _ error: String?) -> Void
typealias ReviewCompletion = (_ movies: [MovieReviewModel]?, _ error: String?) -> Void
typealias MovieCompletion = (_ movies: [MoviesModel]?, _ error: String?) -> Void

protocol MovieDetailServiceProtocol {
    func fetchDetailOfMovie(movieId: Int,
                            completion: @escaping DetailCompletion)
    func fetchSimilarMovies(movieId: Int,
                            completion: @escaping MovieCompletion)
    func fetchCastOfMovie(movieId: Int,
                          completion: @escaping ActorCompletion)
    func fetchReviewsOfMovie(movieId: Int,
                             completion: @escaping ReviewCompletion)
}

class MovieDetailService: MovieDetailServiceProtocol {
    var networkSevice: MovieServiceHelperProtocol

    init(service: MovieServiceHelperProtocol) {
        networkSevice = service
    }

    // Detail of Movie
    func fetchDetailOfMovie(movieId: Int,
                            completion: @escaping DetailCompletion) {
        guard let url = URL(string: "\(APIConstants.baseURL)/\(movieId)?api_key=\(APIConstants.APIToken)") else {
            return completion(nil, nil)
        }

        var urlRequest = URLRequest(url: url, cachePolicy: .useProtocolCachePolicy, timeoutInterval: APIConstants.APITimeout)
        urlRequest.httpMethod = "GET"

        networkSevice.requestModel(urlRequest) { data, error in

            guard let dataResponse = data, error == nil else {
                return completion(nil, error?.localizedDescription ?? "Response error")
            }
            do {
                // here dataResponse received from a network request
                let decoder = JSONDecoder()
                let response = try decoder.decode(MoviesModel.self, from: dataResponse)
                return completion(response, nil)

            } catch let parsingError {
                return completion(nil, parsingError.localizedDescription)
            }
        }
    }

    // Similar Movies
    func fetchSimilarMovies(movieId: Int,
                            completion: @escaping MovieCompletion) {
        guard let url = URL(string: "\(APIConstants.baseURL)/\(movieId)\(APIConstants.movieRelatedURL)api_key=\(APIConstants.APIToken)") else {
            return completion([], nil)
        }

        var urlRequest = URLRequest(url: url, cachePolicy: .useProtocolCachePolicy, timeoutInterval: APIConstants.APITimeout)
        urlRequest.httpMethod = "GET"

        networkSevice.requestModel(urlRequest) { data, error in

            guard let dataResponse = data, error == nil else {
                print(error?.localizedDescription ?? "Response Error")
                return completion(nil, error?.localizedDescription ?? "Response error")
            }
            do {
                // here dataResponse received from a network request
                let decoder = JSONDecoder()
                let response = try decoder.decode(MovieResponse.self, from: dataResponse)
                return completion(response.movies, nil)

            } catch let parsingError {
                print("Error", parsingError)
                return completion(nil, parsingError.localizedDescription)
            }
        }
    }

    // Cast Of Movie
    func fetchCastOfMovie(movieId: Int,
                          completion: @escaping ActorCompletion) {
        guard let url = URL(string: "\(APIConstants.baseURL)/\(movieId)\(APIConstants.movieCastURL)api_key=\(APIConstants.APIToken)") else {
            return completion([], nil)
        }

        var urlRequest = URLRequest(url: url, cachePolicy: .useProtocolCachePolicy, timeoutInterval: APIConstants.APITimeout)
        urlRequest.httpMethod = "GET"

        networkSevice.requestModel(urlRequest) { data, error in

            guard let dataResponse = data, error == nil else {
                return completion(nil, error?.localizedDescription ?? "Response error")
            }
            do {
                // here dataResponse received from a network request
                let decoder = JSONDecoder()
                let response = try decoder.decode(MovieCastResponse.self, from: dataResponse)
                return completion(response.casts, nil)

            } catch let parsingError {
                return completion(nil, parsingError.localizedDescription)
            }
        }
    }

    // Reviews Of Movie
    func fetchReviewsOfMovie(movieId: Int,
                             completion: @escaping ReviewCompletion) {
        guard let url = URL(string: "\(APIConstants.baseURL)/\(movieId)\(APIConstants.movieReviewURL)api_key=\(APIConstants.APIToken)") else {
            return completion([], nil)
        }

        var urlRequest = URLRequest(url: url, cachePolicy: .useProtocolCachePolicy, timeoutInterval: APIConstants.APITimeout)
        urlRequest.httpMethod = "GET"

        networkSevice.requestModel(urlRequest) { data, error in

            guard let dataResponse = data, error == nil else {
                return completion(nil, error?.localizedDescription ?? "Response error")
            }
            do {
                // here dataResponse received from a network request
                let decoder = JSONDecoder()
                let response = try decoder.decode(MovieReviewResponse.self, from: dataResponse)
                return completion(response.reviews, nil)

            } catch let parsingError {
                return completion(nil, parsingError.localizedDescription)
            }
        }
    }
}
