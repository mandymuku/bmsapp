//
//  MovieDetailViewModel.swift
//  BMSApp
//
//  Created by Mukesh on 21/07/19.
//  Copyright © 2019 Mukesh. All rights reserved.
//

import UIKit

protocol MovieDetailViewModelProtocol: UITableViewDataSource {
    // MARK: - Variables

    var movie: MoviesModel! { get set }
    var similarMoviesArr: [MoviesModel]? { get set }
    var castArr: [MovieCastModel]? { get set }
    var reviewArr: [MovieReviewModel]? { get set }

    // MARK: - Methods
    func fetchDetailOfMovie(movieId: Int,
                            completion: @escaping (String?) -> Void)
    func fetchSimilarMovies(movieId: Int,
                            completion: @escaping (String?) -> Void)
    func fetchCastOfMovie(movieId: Int,
                          completion: @escaping (String?) -> Void)
    func fetchReviewsOfMovie(movieId: Int,
                             completion: @escaping (String?) -> Void)
}

class MovieDetailViewModel: NSObject {
    // MARK: - Variables

    var movie: MoviesModel!
    var similarMoviesArr: [MoviesModel]? = [MoviesModel]()
    var castArr: [MovieCastModel]? = [MovieCastModel]()
    var reviewArr: [MovieReviewModel]? = [MovieReviewModel]()

    private let headerCellReuseIdentifier = "MovieHeaderTableViewCell"
    private let infoCellReuseIdentifier = "MovieInfoTableViewCell"
    
    private let movieService: MovieDetailService
    private let movieRouter: MovieDetailRouter
    
    // MARK: - Methods
    
    init(service: MovieDetailService, router: MovieDetailRouter) {
        movieService = service
        movieRouter = router
    }
}

extension MovieDetailViewModel: MovieDetailViewModelProtocol {
    func fetchDetailOfMovie(movieId: Int,
                            completion: @escaping (String?) -> Void) {
        movieService.fetchDetailOfMovie(movieId: movieId) { [weak self] (movie, err) in
            DispatchQueue.main.async {
                self?.movie = movie
                completion(err)
            }
        }
    }
    
    func fetchSimilarMovies(movieId: Int,
                            completion: @escaping (String?) -> Void) {
        movieService.fetchSimilarMovies(movieId: movieId) { [weak self] (similarMoviesArr, err) in
            DispatchQueue.main.async {
                self?.similarMoviesArr = similarMoviesArr
                completion(err)
            }
        }
    }
    
    func fetchCastOfMovie(movieId: Int,
                          completion: @escaping (String?) -> Void) {
        movieService.fetchCastOfMovie(movieId: movieId) { [weak self] (castArr, err) in
            DispatchQueue.main.async {
                self?.castArr = castArr
                completion(err)

            }
        }
    }
    
    func fetchReviewsOfMovie(movieId: Int,
                             completion: @escaping (String?) -> Void) {
        movieService.fetchReviewsOfMovie(movieId: movieId) { [weak self] (reviewArr, err) in
            DispatchQueue.main.async {
                self?.reviewArr = reviewArr
                completion(err)
            }
        }
    }
    
}

extension MovieDetailViewModel: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }

    func tableView(_ tableView: UITableView,
                   numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 1
        case 1:
            return castArr?.count == 0 ? 0 : 1
        case 2:
            return reviewArr?.count == 0 ? 0 : 1
        case 3:
            return similarMoviesArr?.count == 0 ? 0 : 1
        default:
            return 1
        }
    }

    func tableView(_ tableView: UITableView,
                   cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0: // header info
            if let cell = tableView.dequeueReusableCell(withIdentifier: headerCellReuseIdentifier, for: indexPath) as? MovieHeaderTableViewCell {
                cell.selectionStyle = .none
                cell.configireMovieHeaderCell(model: movie)
                return cell
            }

        case 1: // cast
            if let cell = tableView.dequeueReusableCell(withIdentifier: infoCellReuseIdentifier, for: indexPath) as? MovieInfoTableViewCell {
                cell.selectionStyle = .none
                if let arr = castArr {
                    cell.configureCastList(arr: arr)
                }
                return cell
            }

        case 2: // review
            if let cell = tableView.dequeueReusableCell(withIdentifier: infoCellReuseIdentifier, for: indexPath) as? MovieInfoTableViewCell {
                cell.selectionStyle = .none
                if let arr = reviewArr {
                    cell.configureReviewList(arr: arr)
                }
                return cell
            }

        case 3: // similar
            if let cell = tableView.dequeueReusableCell(withIdentifier: infoCellReuseIdentifier, for: indexPath) as? MovieInfoTableViewCell {
                cell.selectionStyle = .none
                if let arr = similarMoviesArr {
                    cell.configureSimilarMovieList(arr: arr)
                }
                return cell
            }
        default:
            break
        }

        return UITableViewCell()
    }
}
