//
//  MovieHeaderTableViewCell.swift
//  MovieApp
//
//  Created by Mukesh on 21/07/19.
//  Copyright © 2019 Mukesh. All rights reserved.
//

import UIKit

class MovieHeaderTableViewCell: UITableViewCell {
    
    @IBOutlet weak var imgMoviePoster: UIImageView!
    @IBOutlet weak var lblMovieTitle: UILabel!
    @IBOutlet weak var lblMovieOverview: UILabel!
    @IBOutlet weak var lblMovieReleaseDate: UILabel!
    @IBOutlet weak var lblMovieRating: UILabel!
    @IBOutlet weak var lblMovieRunningTime: UILabel!
    
    @IBOutlet weak var lblMovieGenre: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    func configireMovieHeaderCell(model : MoviesModel) {
        
        if let temp = model.movieRating {
            lblMovieRating.text = "\(temp)/10"
        }
        
        if let temp = model.movieRuntime {
            lblMovieRunningTime.text = "\(temp / 60)hr \(temp % 60)min" //minutes / 60, (minutes % 60)
        }
        
        if let temp = model.movieReleaseDate {
            lblMovieReleaseDate.text = "Release Date: \(temp)"
        }
        
        if let path = model.moviePoster {
            imgMoviePoster.loadImageUsingCacheWithUrlString("https://image.tmdb.org/t/p/w780\(path)")
        }
        
        if let name = model.movieName {
            lblMovieTitle.text = name
        }
        
        if let temp = model.movieOverview {
            lblMovieOverview.text = "Overview: \(temp)"
        }
        
        if let temp = model.movieGenres {
            var genres = ""
            for (index, genre) in temp.enumerated() {
                genres = genres + (genre.name ?? "") + ((index == temp.count - 1) ? "" : ", ")
            }
            lblMovieGenre.text = "Genre: \(genres)"
        }

    }
}
