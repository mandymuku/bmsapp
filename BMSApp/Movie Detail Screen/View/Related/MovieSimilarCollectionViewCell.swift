//
//  MovieSimilarCollectionViewCell.swift
//  MovieApp
//
//  Created by Mukesh on 21/07/19.
//  Copyright © 2019 Mukesh. All rights reserved.
//

import UIKit

class MovieSimilarCollectionViewCell: UICollectionViewCell {
    
    
    @IBOutlet weak var imgMovie: UIImageView!
    
    @IBOutlet weak var viewGradient: UIView!
    @IBOutlet weak var lblMovieTitle: UILabel!
    
    private let gradientLayer: CAGradientLayer = {
        let gradient = CAGradientLayer()
        gradient.colors = [UIColor.black.cgColor, UIColor.clear.cgColor]
        gradient.startPoint = CGPoint(x: 1, y: 1)
        gradient.endPoint = CGPoint(x: 1, y: 0)
        gradient.type = .axial
        return gradient
    }()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        layer.shadowOffset = CGSize.zero
        layer.shadowOpacity = 0.5
        layer.shadowRadius = 6
        layer.shadowColor = UIColor.black.cgColor
        clipsToBounds = false
        
        gradientLayer.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width - 20, height: viewGradient.bounds.height)
        self.viewGradient.layer.insertSublayer(gradientLayer, at: 0)

    }
    
    func configureMovieCell(model : MoviesModel) {
        
        if let path = model.moviePoster {
            imgMovie.loadImageUsingCacheWithUrlString("https://image.tmdb.org/t/p/w342\(path)")
        }
        
        if let name = model.movieName {
            lblMovieTitle.text = name
        }
    }

}
