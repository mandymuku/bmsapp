//
//  MovieInfoTableViewCell.swift
//  MovieApp
//
//  Created by Mukesh on 21/07/19.
//  Copyright © 2019 Mukesh. All rights reserved.
//

import UIKit

class MovieInfoTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblInfoType: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!{
        didSet{
            
            collectionView.register(UINib(nibName: actorCellReuseIdentifier, bundle: nil), forCellWithReuseIdentifier: actorCellReuseIdentifier)
            collectionView.register(UINib(nibName: reviewCellReuseIdentifier, bundle: nil), forCellWithReuseIdentifier: reviewCellReuseIdentifier)
            collectionView.register(UINib(nibName: relatedCellReuseIdentifier, bundle: nil), forCellWithReuseIdentifier: relatedCellReuseIdentifier)
            collectionView.contentInset = UIEdgeInsets(top: 0, left: 8, bottom: 0, right: 0)

        }
    }
    
    let actorCellReuseIdentifier = "MovieActorCollectionViewCell"
    let reviewCellReuseIdentifier = "MovieReviewCollectionViewCell"
    let relatedCellReuseIdentifier = "MovieSimilarCollectionViewCell"
    var cellType = 0 // 0 for cast , 1 review ,  2 related
    var castArr = [MovieCastModel]()
    var similarMoviesArr = [MoviesModel]()
    var reviewsArr = [MovieReviewModel]()

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configureCastList(arr : [MovieCastModel]) {
        lblInfoType.text = "Casts"
        self.castArr = arr
        cellType = 0
        collectionView.reloadData()
    }
    
    func configureReviewList(arr : [MovieReviewModel]) {
        lblInfoType.text = "User Reviews"
        self.reviewsArr = arr
        cellType = 1
        collectionView.reloadData()

    }
    
    func configureSimilarMovieList(arr : [MoviesModel]) {
        lblInfoType.text = "Similar Movies"
        self.similarMoviesArr = arr
        cellType = 2
        collectionView.reloadData()

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

extension MovieInfoTableViewCell : UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 20)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 15
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        switch cellType {
        case 0:
            return CGSize(width : 150, height : 200)
        case 1:
            return CGSize(width : 220, height : 170)
        case 2:
            return CGSize(width : 150, height : 200)
        default:
            return CGSize.zero
        }
    }
    

}

extension MovieInfoTableViewCell : UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch cellType {
        case 0:
            return castArr.count
        case 1:
            return reviewsArr.count
        case 2:
            return similarMoviesArr.count
        default:
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        switch cellType {
        case 0:
            if let cell = collectionView.dequeueReusableCell(withReuseIdentifier:actorCellReuseIdentifier, for: indexPath) as? MovieActorCollectionViewCell{
                
                let cast = castArr[indexPath.item]
                cell.configureActorCell(actor: cast)
                return cell
            }
        case 1:
            if let cell = collectionView.dequeueReusableCell(withReuseIdentifier:reviewCellReuseIdentifier, for: indexPath) as? MovieReviewCollectionViewCell{
                
                let review = reviewsArr[indexPath.item]
                cell.configureReviewCell(model: review)
                return cell
            }
        case 2:
            if let cell = collectionView.dequeueReusableCell(withReuseIdentifier:relatedCellReuseIdentifier, for: indexPath) as? MovieSimilarCollectionViewCell{
                
                let movie = similarMoviesArr[indexPath.item]
                cell.configureMovieCell(model: movie)
                return cell
            }
        default:
            break
        }
        
        return UICollectionViewCell()
    }
}
