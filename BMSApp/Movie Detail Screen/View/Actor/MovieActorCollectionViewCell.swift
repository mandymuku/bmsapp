//
//  MovieActorCollectionViewCell.swift
//  MovieApp
//
//  Created by Mukesh on 21/07/19.
//  Copyright © 2019 Mukesh. All rights reserved.
//

import UIKit

class MovieActorCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imgActor: UIImageView!
    
    @IBOutlet weak var lblActorRealName: UILabel!
    
    @IBOutlet weak var lblActorReelName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        layer.shadowOffset = CGSize.zero
        layer.shadowOpacity = 0.5
        layer.shadowRadius = 6
        layer.shadowColor = UIColor.black.cgColor
        clipsToBounds = false
        
    }
    
    func configureActorCell(actor : MovieCastModel) {
        
        if let path = actor.actorImage {
            imgActor.loadImageUsingCacheWithUrlString(("https://image.tmdb.org/t/p/w185\(path)"))
        }
        
        if let temp = actor.actorName {
            lblActorRealName.text = temp
        }

        
        if let temp = actor.characterName {
            lblActorReelName.text = "as \(temp)"
        }

    }

}
