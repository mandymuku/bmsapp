//
//  MovieReviewCollectionViewCell.swift
//  MovieApp
//
//  Created by Mukesh on 21/07/19.
//  Copyright © 2019 Mukesh. All rights reserved.
//

import UIKit

class MovieReviewCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var lblUserReview: UILabel!
    @IBOutlet weak var lblUserName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        layer.shadowOffset = CGSize.zero
        layer.shadowOpacity = 0.5
        layer.shadowRadius = 6
        layer.shadowColor = UIColor.black.cgColor
        clipsToBounds = false
    }

    func configureReviewCell(model : MovieReviewModel) {
        
        if let temp = model.author {
            lblUserName.text = temp
        }
        
        if let temp = model.content {
            lblUserReview.text = temp
        }
    }
}
