//
//  MovieDetailVC.swift
//  BMSApp
//
//  Created by Mukesh on 21/07/19.
//  Copyright © 2019 Mukesh. All rights reserved.
//

import UIKit

class MovieDetailVC: UIViewController, StoryboardableInitProtocol, ViewModelProtocol {
    
    //MARK: - Variables
    internal var viewModel: MovieDetailViewModelProtocol!
    @IBOutlet weak var tableView: UITableView!{
        didSet{
            
            tableView.register(UINib(nibName: headerCellReuseIdentifier, bundle: nil), forCellReuseIdentifier: headerCellReuseIdentifier)
            tableView.register(UINib(nibName: infoCellReuseIdentifier, bundle: nil), forCellReuseIdentifier: infoCellReuseIdentifier)
            tableView.estimatedRowHeight = 400
            tableView.tableFooterView = UIView(frame: CGRect.zero)
            tableView.delegate = self
            tableView.dataSource = viewModel
        }
    }
    @IBOutlet weak var closeBut: UIButton! {
        didSet{
            closeBut.addTarget(self, action: #selector(dismissAction), for: .touchUpInside)
            closeBut.backgroundColor = UIColor(white: 0, alpha: 0.5)
            closeBut.layer.cornerRadius = closeBut.frame.height/2
        }
    }
    private let headerCellReuseIdentifier = "MovieHeaderTableViewCell"
    private let infoCellReuseIdentifier = "MovieInfoTableViewCell"
    
    // MARK: - View Life cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        assert(viewModel != nil, "ViewModel cannot be nil")
        loadDetailOfMovie()
    }
    
    // MARK: - Helper Methods

    func loadDetailOfMovie() {
        if let id = viewModel.movie.movieId {
            viewModel.fetchDetailOfMovie(movieId: id) { [weak self] (errMsg) in
                if let err = errMsg {
                    self?.showAlert(msg: err)
                    return
                }
                self?.tableView.reloadData()
                self?.loadCastsOfMovie()
            }
        }
    }
    
    func loadCastsOfMovie() {
        if let id = viewModel.movie.movieId {
            viewModel.fetchCastOfMovie(movieId: id) { [weak self] (errMsg) in
                if let err = errMsg {
                    self?.showAlert(msg: err)
                    return
                }
                self?.tableView.reloadSections(IndexSet(integer: 1), with: .automatic)
                self?.loadReviewslOfMovie()
            }
        }
    }
    
    func loadReviewslOfMovie() {
        if let id = viewModel.movie.movieId {
            viewModel.fetchReviewsOfMovie(movieId: id) { [weak self] (errMsg) in
                if let err = errMsg {
                    self?.showAlert(msg: err)
                    return
                }
                self?.tableView.reloadSections(IndexSet(integer: 2), with: .automatic)
                self?.loadSimilarMovies()
            }
        }
    }
    
    
    func loadSimilarMovies() {
        if let id = viewModel.movie.movieId {
            viewModel.fetchSimilarMovies(movieId: id) { [weak self] (errMsg) in
                if let err = errMsg {
                    self?.showAlert(msg: err)
                    return
                }
                self?.tableView.reloadSections(IndexSet(integer: 3), with: .automatic)
            }
        }
    }
    
    @objc func dismissAction() {
        self.dismiss(animated: true, completion: nil)
    }

}
extension MovieDetailVC : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView,
                   heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        switch indexPath.section {
        case 1:
            return 276
        case 2:
            return 250
        case 3:
            return 276
        default:
            return 390 + (viewModel.movie.movieOverview ?? "").getStringHeight(width: view.bounds.width - 32, style: UIFont.systemFont(ofSize: 17))
        }
    }
}
