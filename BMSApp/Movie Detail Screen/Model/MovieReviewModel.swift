//
//  MovieReviewModel.swift
//  BMSApp
//
//  Created by Mukesh on 21/07/19.
//  Copyright © 2019 Mukesh. All rights reserved.
//

import UIKit

struct MovieReviewModel : Codable {
    
    var id : String?
    var author : String?
    var content : String?
    
}


struct MovieReviewResponse : Codable {
    
    var reviews : [MovieReviewModel]?
    
    enum CodingKeys : String, CodingKey {
        case reviews = "results"
    }
    
}
