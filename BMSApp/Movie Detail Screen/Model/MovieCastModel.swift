//
//  MovieCastModel.swift
//  BMSApp
//
//  Created by Mukesh on 21/07/19.
//  Copyright © 2019 Mukesh. All rights reserved.
//

import UIKit

struct MovieCastModel : Codable {
    
    var characterId : Int?
    var characterName : String?
    var actorImage : String?
    var actorName : String?
    
    enum CodingKeys : String, CodingKey {
        
        case characterId = "cast_id"
        case characterName = "character"
        case actorImage = "profile_path"
        case actorName = "name"
        
    }
    
}


struct MovieCastResponse : Codable {
    
    var casts : [MovieCastModel]?
    
    enum CodingKeys : String, CodingKey {
        case casts = "cast"
    }
    
}
