//
//  MovieSynopsisModel.swift
//  BMSApp
//
//  Created by Mukesh on 21/07/19.
//  Copyright © 2019 Mukesh. All rights reserved.
//

import UIKit

struct MovieSynopsisModel: Codable {
    
    var movieId : Int?
    var movieTitle : String?
    var movieOverview : String?
    var moviePoster : String?
    var movieReleaseDate : String?
    
    
    enum CodingKeys : String, CodingKey {
        
        case movieId = "id"
        case movieTitle = "original_title"
        case movieOverview = "overview"
        case moviePoster = "poster_path"
        case movieReleaseDate = "release_date"
    }
}
