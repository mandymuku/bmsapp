//
//  MovieListService.swift
//  BMSApp
//
//  Created by Mukesh on 21/07/19.
//  Copyright © 2019 Mukesh. All rights reserved.
//

import UIKit

protocol MovieListServiceProtocol {
    func fetchMovieList(page: Int,
                        completion: @escaping (MovieResponse?, String?) -> Void)
}

class MovieListService: MovieListServiceProtocol {
    var networkSevice: MovieServiceHelperProtocol

    init(service: MovieServiceHelperProtocol) {
        networkSevice = service
    }

    func fetchMovieList(page: Int,
                        completion: @escaping (MovieResponse?, String?) -> Void) {
        guard let url = URL(string: "\(APIConstants.baseURL)\(APIConstants.movieURL)api_key=\(APIConstants.APIToken)&page=\(page)") else {
            return completion(nil, "Invalid Request")
        }

        var urlRequest = URLRequest(url: url, cachePolicy: .useProtocolCachePolicy, timeoutInterval: APIConstants.APITimeout)
        urlRequest.httpMethod = "GET"

        networkSevice.requestModel(urlRequest) { data, error in
            guard let dataResponse = data, error == nil else {
                return completion(nil, error?.localizedDescription ?? "Response Error")
            }
            do {
                // here dataResponse received from a network request
                let decoder = JSONDecoder()
                let response = try decoder.decode(MovieResponse.self, from: dataResponse)
                return completion(response, nil)

            } catch let parsingError {
                return completion(nil, parsingError.localizedDescription)
            }
        }
    }
}
