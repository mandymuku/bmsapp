//
//  MovieGenre.swift
//  BMSApp
//
//  Created by Mukesh on 21/07/19.
//  Copyright © 2019 Mukesh. All rights reserved.
//

import UIKit

struct MovieGenre : Codable {
    
    var id : Int?
    var name : String?
    
    
    enum CodingKeys : String, CodingKey {
        
        case id = "id"
        case name = "name"
    }
    
}
