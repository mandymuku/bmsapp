//
//  MovieModel.swift
//  BMSApp
//
//  Created by Mukesh on 21/07/19.
//  Copyright © 2019 Mukesh. All rights reserved.
//

import UIKit

struct MoviesModel : Codable {
    
    var movieId : Int?
    var movieName : String?
    var moviePoster : String?
    var movieRuntime : Int?
    var movieRating : Double?
    var movieReleaseDate : String?
    var movieOverview : String?
    var movieGenres : [MovieGenre]?
    var movieOriginalLanguage : String?
    
    enum CodingKeys : String, CodingKey {
        
        case movieId = "id"
        case movieName = "original_title"
        case moviePoster = "backdrop_path"//"poster_path"
        case movieRating = "vote_average"
        case movieReleaseDate = "release_date"
        case movieOverview = "overview"
        case movieRuntime = "runtime"
        case movieGenres = "genres"
        case movieOriginalLanguage = "original_language"
        
    }
    
}



struct MovieResponse : Codable {
    
    var movies : [MoviesModel]?
    
    enum CodingKeys : String, CodingKey {
        case movies = "results"
    }
    
}
