//
//  MovieListRouter.swift
//  BMSApp
//
//  Created by Mukesh on 21/07/19.
//  Copyright © 2019 Mukesh. All rights reserved.
//

import UIKit

protocol MovieListRouterProtocol: AppRouter {
    func showDetailOfMovie(movie: MoviesModel)
    func showNoResultsAlert()
}

class MovieListRouter: MovieListRouterProtocol {
    var viewController: UIViewController?
}

extension MovieListRouter {
    func showDetailOfMovie(movie: MoviesModel) {
        guard let vc = viewController else {
            return
        }
        let serviceHelper = MovieServiceHelper()
        let movieService = MovieDetailService(service: serviceHelper)
        let movieRouter = MovieDetailRouter()
        let viewModel = MovieDetailViewModel(service: movieService, router: movieRouter)
        viewModel.movie = movie
        let movieDetaiVc = MovieDetailVC.instantiateViewController(storyBoardName: "Main",
                                                                   identifier: MovieDetailVC.self) as! MovieDetailVC
        movieDetaiVc.viewModel = viewModel
        vc.present(movieDetaiVc, animated: true, completion: nil)
    }
    
    func showNoResultsAlert() {
        guard let vc = viewController else {
            return
        }
        vc.showAlert(msg: "No results")
    }
}
