//
//  MovieCollectionViewCell.swift
//  BMSApp
//
//  Created by Mukesh on 21/07/19.
//  Copyright © 2019 Mukesh. All rights reserved.
//

import UIKit

class MovieCollectionViewCell: UICollectionViewCell {
    @IBOutlet var imgMovie: UIImageView!
    @IBOutlet var viewGradient: UIView!
    @IBOutlet var lblMovieName: UILabel!
    @IBOutlet var butBookNow: UIButton!
    @IBOutlet var lblReleaseDate: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        layer.shadowOffset = CGSize.zero
        layer.shadowOpacity = 0.5
        layer.shadowRadius = 6
        layer.shadowColor = UIColor.black.cgColor
        clipsToBounds = false
    }

    func configureMovieCell(model: MoviesModel) {
        imgMovie.loadImageUsingCacheWithUrlString("https://image.tmdb.org/t/p/w780\(model.moviePoster ?? "")")
        lblMovieName.text = model.movieName
        lblReleaseDate.text = " Release Date: \(model.movieReleaseDate ?? "")"
    }
}

extension MovieCollectionViewCell {
    override var isSelected: Bool {
        didSet {
            if isSelected {
                UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseOut, animations: {
                    self.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
                }, completion: nil)
            } else {
                UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseOut, animations: {
                    self.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                }, completion: nil)
            }
        }
    }

    override var isHighlighted: Bool {
        didSet {
            if isHighlighted {
                UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseOut, animations: {
                    self.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
                }, completion: nil)
            } else {
                UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseOut, animations: {
                    self.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                }, completion: nil)
            }
        }
    }
}
