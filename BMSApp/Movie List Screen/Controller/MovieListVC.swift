//
//  MovieListVC.swift
//  BMSApp
//
//  Created by Mukesh on 21/07/19.
//  Copyright © 2019 Mukesh. All rights reserved.
//

import UIKit

class MovieListVC: UIViewController, StoryboardableInitProtocol, ViewModelProtocol {
    // MARK: - Variables

    internal var viewModel: MovieListViewModelProtocol!
    private let cellReuseIdentifier = "MovieCollectionViewCell"
    private var pageNum = 1
    private var pagination = true

    lazy var loader: UIActivityIndicatorView = {
        let temp = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.gray)
        temp.center = view.center
        view.addSubview(temp)
        return temp
    }()

    lazy var searchBar: UISearchBar = {
        let temp = UISearchBar()
        temp.placeholder = "Search Movies..."
        self.navigationItem.titleView = temp
        return temp
    }()

    @IBOutlet var collectionView: UICollectionView! {
        didSet {
            collectionView.dataSource = viewModel
            collectionView.delegate = self
            collectionView.register(UINib(nibName: cellReuseIdentifier, bundle: nil), forCellWithReuseIdentifier: cellReuseIdentifier)
            collectionView.contentInset = UIEdgeInsets(top: 12, left: 0, bottom: 50, right: 0)
        }
    }

    // MARK: - View Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        assert(viewModel != nil, "ViewModel cannot be nil")
        searchBar.delegate = self
        title = "Now Playing"
        loadMovies()
    }

    func loadMovies() {
        loader.startAnimating()
        viewModel.fetchMovies(pageNum: pageNum) { [weak self] movieCount, err in
            if let errorMsg = err {
                self?.showAlert(msg: errorMsg)
            } else {
                self?.insertNewRows(movieCount: movieCount)
            }
            self?.loader.stopAnimating()
        }
    }
}

extension MovieListVC: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView,
                        didSelectItemAt indexPath: IndexPath) {
        viewModel.loadMovieDetail(index: indexPath.item)
        collectionView.deselectItem(at: indexPath, animated: true)
    }

    func insertNewRows(movieCount: Int) {
        pagination = movieCount == 0 ? true : false
        guard let totalMovieCount = viewModel.movies?.count else {
            return
        }
        let oldMovieCount = totalMovieCount - movieCount
        var indexPaths = [IndexPath]()

        // new indexpaths
        for item in oldMovieCount ..< totalMovieCount {
            let path = IndexPath(item: item, section: 0)
            indexPaths.append(path)
        }
        // update collectionview
        if pageNum == 1 {
            collectionView.reloadData()

        } else {
            collectionView.insertItems(at: indexPaths)
        }
    }
}

extension MovieListVC: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
    }

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 15
    }

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width :(view.bounds.width) - 20,
                      height : 350)
    }

    func collectionView(_ collectionView: UICollectionView,
                        willDisplay cell: UICollectionViewCell,
                        forItemAt indexPath: IndexPath) {
        guard let arr = viewModel.movies else {
            return
        }
        if indexPath.item == arr.count - 1 && !pagination && !viewModel.isSearchEnabled!  {
            pagination = true
            pageNum += 1
            loadMovies()
        }
    }
}

// MARK: - Search Delegate

extension MovieListVC: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }

    func searchBar(_ searchBar: UISearchBar,
                   textDidChange searchText: String) {
        guard let searchText = searchBar.text else {
            return
        }
        viewModel.filterSearch(searchText: searchText)
        collectionView.reloadData()
    }

    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        searchBar.showsCancelButton = false
        viewModel.isSearchEnabled = false
        collectionView.reloadData()
    }

    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = true
        viewModel.isSearchEnabled = true
    }
}
