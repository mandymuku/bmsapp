//
//  MovieListViewModel.swift
//  BMSApp
//
//  Created by Mukesh on 21/07/19.
//  Copyright © 2019 Mukesh. All rights reserved.
//

import UIKit

protocol MovieListViewModelProtocol: UICollectionViewDataSource {
    // MARK: - Variables

    var movies: [MoviesModel]? { get set }
    var searchMovieArr: [MoviesModel]? { get set }
    var isSearchEnabled: Bool? { get set }

    // MARK: - Methods

    func fetchMovies(pageNum: Int, completion: @escaping (Int, String?) -> Void)
    func loadMovieDetail(index: Int)
    func filterSearch(searchText: String)
}

class MovieListViewModel: NSObject {
    // MARK: - Variables

    private let movieService: MovieListService
    private let movieRouter: MovieListRouter
    var movies: [MoviesModel]? = [MoviesModel]()
    var searchMovieArr: [MoviesModel]? = [MoviesModel]()
    var isSearchEnabled: Bool? = false
    private let cellReuseIdentifier = "MovieCollectionViewCell"

    // MARK: - Methods

    init(service: MovieListService, router: MovieListRouter) {
        movieService = service
        movieRouter = router
    }
}

extension MovieListViewModel: MovieListViewModelProtocol {
    func fetchMovies(pageNum: Int,
                     completion: @escaping (Int, String?) -> Void) {
        movieService.fetchMovieList(page: pageNum) { [weak self] response, errorMsg in
            DispatchQueue.main.async {
                if let arr = response?.movies {
                    self?.movies?.append(contentsOf: arr)
                    completion(arr.count, nil)
                } else {
                    completion(0, errorMsg)
                }
            }
        }
    }
}

extension MovieListViewModel {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    func collectionView(_ collectionView: UICollectionView,
                        numberOfItemsInSection section: Int) -> Int {
        guard let searchBool = isSearchEnabled else {
            return 0
        }
        return (searchBool ? searchMovieArr?.count : movies?.count) ?? 0
    }

    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellReuseIdentifier, for: indexPath) as? MovieCollectionViewCell {
            if let searchBool = isSearchEnabled, !searchBool {
                if let movie = movies?[indexPath.item] {
                    cell.configureMovieCell(model: movie)
                }
            }else if let movie = searchMovieArr?[indexPath.item] {
                cell.configureMovieCell(model: movie)

            }
            return cell
        }
        return UICollectionViewCell()
    }
}

extension MovieListViewModel {
    func loadMovieDetail(index: Int) {
        guard let searchBool = isSearchEnabled else {
            return
        }
        if let movie = movies?[index], searchBool == false {
            movieRouter.showDetailOfMovie(movie: movie)
        }else if let movie = searchMovieArr?[index] {
            movieRouter.showDetailOfMovie(movie: movie)
        }
    }
}

// MARK: - Search Logic

extension MovieListViewModel {
    func filterSearch(searchText: String) {
        guard let moviesArr = movies else {
            return
        }
        let searchArr = moviesArr.filter { (model) -> Bool in
            let tempArr = model.movieName?.components(separatedBy: " ") ?? []
            for word in tempArr {
                if word.hasPrefix(searchText) {
                    return true
                }
            }

            return false
        }
        self.searchMovieArr = searchArr
        if searchArr.count == 0 {
            movieRouter.showNoResultsAlert()
        }
    }
}
