//
//  AppRouter.swift
//  BMSApp
//
//  Created by Mukesh on 21/07/19.
//  Copyright © 2019 Mukesh. All rights reserved.
//

import UIKit

protocol AppRouter {
    var viewController : UIViewController? {get set}
}
