//
//  APIHelper.swift
//  BMSApp
//
//  Created by Mukesh on 21/07/19.
//  Copyright © 2019 Mukesh. All rights reserved.
//

import UIKit

protocol MovieServiceHelperProtocol {
    func requestModel(_ request: URLRequest,
                      completion: @escaping (Data?, Error?) -> Void)
}

class MovieServiceHelper: MovieServiceHelperProtocol {
    func requestModel(_ request: URLRequest,
                      completion: @escaping (Data?, Error?) -> Void) {
        let task = URLSession.shared.dataTask(with: request) { data, _, error in
            completion(data, error)
        }
        task.resume()
    }
}
