//
//  StoryBoardInit.swift
//  BMSApp
//
//  Created by Mukesh on 21/07/19.
//  Copyright © 2019 Mukesh. All rights reserved.
//

import UIKit

protocol ViewModelProtocol {
    associatedtype ViewModelType
    var viewModel: ViewModelType! {get set}
}
protocol StoryboardableInitProtocol {
    
}

extension StoryboardableInitProtocol {
    
    static func instantiateViewController(storyBoardName: String,
                                          identifier : UIViewController.Type) -> UIViewController {
        let viewController = UIStoryboard(name: storyBoardName,
                                          bundle: nil).instantiateViewController(withIdentifier: String(describing: identifier.self))
        return viewController
    }
}
