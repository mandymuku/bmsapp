//
//  APIConstants.swift
//  BMSApp
//
//  Created by Mukesh on 21/07/19.
//  Copyright © 2019 Mukesh. All rights reserved.
//
import UIKit

enum APIConstants {
    
    //Networking Constant
    static var APIToken = "8eac22f4c24d01c480e4d99fef2edfc3"
    static var APITimeout : TimeInterval = 30
    static var baseURL = "https://api.themoviedb.org/3/movie"
    
    
    //All URL's
    static var movieURL = "/now_playing?"
    static var movieCastURL = "/credits?"
    static var movieRelatedURL = "/similar?"
    static var movieReviewURL = "/reviews?"
    
}
