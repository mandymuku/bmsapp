
//
//  ViewControllerEx.swift
//  BMSApp
//
//  Created by Mukesh on 21/07/19.
//  Copyright © 2019 Mukesh. All rights reserved.
//

import UIKit

extension UIViewController {
    func showAlert(msg : String){
        let alertVc = UIAlertController(title: "BMSApp", message: msg, preferredStyle: .alert)
        let alertAction = UIAlertAction(title: "OK", style: .default) { (action) in
            self.dismiss(animated: true, completion: nil)
        }
        alertVc.addAction(alertAction)
        self.present(alertVc, animated: true, completion: nil)
    }
}
