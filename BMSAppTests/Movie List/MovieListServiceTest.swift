//
//  MovieListServiceTest.swift
//  BMSAppTests
//
//  Created by mukesh on 22/7/19.
//  Copyright © 2019 BookMyShow. All rights reserved.
//

import UIKit
@testable import BMSApp
import XCTest

class MovieListServiceTest: XCTestCase {
    var movieListService: MovieListServiceProtocol!
    
    override func setUp() {
        let helper = MockServiceHelper()
        movieListService = MovieListService(service: helper)
    }
    
    override func tearDown() {
    }
    
    func testContactListAPI() {
        let networkExpectation = expectation(description: "Movie List Testing")
        movieListService.fetchMovieList(page: 1) { (response, error) in
            XCTAssertNil(error, "Error\(error ?? "Something went wrong")")
            XCTAssertNotNil(response?.movies, "No response")
            XCTAssertNotEqual(response?.movies?.count, 0, "Movie list is empty")
            networkExpectation.fulfill()
        }
        waitForExpectations(timeout: 5.0, handler: nil)
    }
}

