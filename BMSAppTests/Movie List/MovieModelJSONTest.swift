//
//  MovieModelJSONTest.swift
//  BMSAppTests
//
//  Created by mukesh on 22/7/19.
//  Copyright © 2019 BookMyShow. All rights reserved.
//

@testable import BMSApp
import UIKit
import XCTest

class MovieModelJSONTest: XCTestCase {
    func testJSONMapping() throws {
        let bundle = Bundle(for: type(of: self))

        guard let url = bundle.url(forResource: "nowplaying", withExtension: "json") else {
            XCTFail("Missing file: nowplaying.json")
            return
        }

        let json = try Data(contentsOf: url)
        do {
            // here dataResponse received from a network request
            let decoder = JSONDecoder()
            let movie = try decoder.decode(MoviesModel.self, from: json)
            XCTAssertEqual(movie.movieName, "The Lion King")
            XCTAssertEqual(movie.movieId, 420818)
            XCTAssertEqual(movie.movieReleaseDate, "2019-07-12")
            XCTAssertEqual(movie.movieOverview, "Simba idolises his father, King Mufasa, and takes to heart his own royal destiny. But not everyone in the kingdom celebrates the new cub's arrival. Scar, Mufasa's brother—and former heir to the throne—has plans of his own. The battle for Pride Rock is ravaged with betrayal, tragedy and drama, ultimately resulting in Simba's exile. With help from a curious pair of newfound friends, Simba will have to figure out how to grow up and take back what is rightfully his.")
            XCTAssertEqual(movie.movieRating, 7.1)

        } catch let parsingError {
            XCTAssertThrowsError(parsingError)
        }
    }
}
