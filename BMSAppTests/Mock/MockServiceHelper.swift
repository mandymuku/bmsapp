//
//  MockServiceHelper.swift
//  BMSAppTests
//
//  Created by mukesh on 22/7/19.
//  Copyright © 2019 BookMyShow. All rights reserved.
//

import UIKit

@testable import BMSApp
import UIKit

class MockServiceHelper: MovieServiceHelperProtocol {
    func requestModel(_ request: URLRequest,
                      completion: @escaping (Data?, Error?) -> Void)  {
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            completion(data, error)
        }
        task.resume()
    }
}
